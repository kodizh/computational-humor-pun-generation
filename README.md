# Pun Generation

This program generates puns from sentences. Before any use, modules have to be installed.

## 1. External modules

### Libraries

This program has been developed with Python 3.8.10

You will need to install *(mostly with a pip command)* the following libraries if it's not already done :

- pandas
- re
- colorama
- regex
- os
- JSON
- fasttext
- NumPy
- math

### Fasttext [FR]

This program uses the French model of fasttext. you have to download it here: https://fasttext.cc/docs/en/crawl-vectors.html

Scroll the page until the "French" row. You'll need the "bin" file. 

Once this is done, make sure that the "vecfr" folder is at the root of the project (see file tree diagram)

### Lexique 3

This program uses "Lexique 3", download it here: http://www.lexique.org/

Once downloaded, make sure that the Lexique383 folder is at the root of the project (see file tree diagram)

### File Tree Diagram
```
>project_root
  >pun_genV1
    >pg_script.py
    >semantic_searcher.py
    >topic_j1.py
    >setup.json
  >vecfr
    >cc.fr.300.bin
  >Lexique383
    >Lexique383.tsv
```
Please, make sure to respect this tree. Also, note that every path in the code is a relative path. Your *project_root* can be anywhere.

## Setup

In ***pun_genV1/setup.json*** you have to give some information to the program.

- ***API_KEY***: In this field, you have to give your *AI21studio* API key. First, make sure to create an account  (here : https://studio.ai21.com/) if you don't have one. Then go to the top right of your screen and click on the **profile logo -> Account**. Now you can copy your API key and put it in *setup.json*.

- ***path_to_data***: This field is only used when you want to give a dataset as input. Give the path to the file. The file must be a JSON one, with a ***'text'*** field. The column name is fixed and must be ***'text'***.

- ***path_to_output***: This field is always needed because even with one sentence, the program will create an output *JSON* file. Make sure to indicate a path to the location you want. **The path must contain the file name !**

## Program Run

When you execute the ***pg_scriptV1*** *(that is the main script)*, in the terminal, you will need to type 1 or 2!

- **1**: You want to run the program with only one sentence that you will type in the terminal

- **2**: You have a data set, and you are ready to run the program on the whole data set. *(obviously, paths have been set up)*

- **Other characters**: Wrong entry, the program end.

Note that your API key is limited in the number of requests you are authorized in., and also the number of tokens generated. By default, the J1 model used is the *Jumbo* one, if you reach the limitation with this one, you can change it in the request URL (script: *topic_j1.py*).

However, don't panic, if you reach the limit in the middle of your build, each result is dynamically added to the output file. The generation will stop, but you won't lose your results or your time.

**Now you are ready to use the French pun generator !**
