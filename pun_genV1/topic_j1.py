import pandas as pd
import requests
from transformers import DataCollator

def topic(sentence_df,YOUR_API_KEY):
    
    topics_list = []
    topics = requests.post("https://api.ai21.com/studio/v1/j1-grande/complete",
        headers={"Authorization": "Bearer "+YOUR_API_KEY},
        json={
            "prompt": "Changez le contexte de la phrase selon le paronyme remplaçant.\n--\nphrase: L'homme est tombé, il a cassé un verre.\nmot à remplacer: verre\nparonyme: vers\nnouveau contexte du paronyme remplaçant: Le poète\n--\nphrase: Il a du se confiné, il a pris du poids.\nmot à remplacer: poids\nparonyme: pois\nnouveau contexte du paronyme remplaçant: Le maraicher\n--\nphrase: Tu devrais faire attention à ce que tu mange, ce n'est pas très sain.\nmot à remplacer: sain\nparonyme: saint\nnouveau contexte du paronyme remplaçant: le pape\n--\nphrase: Après la fête, il a passé le balai.\nmot à remplacer: balai\nparonyme: ballet\nnouveau contexte du paronyme remplaçant: le danseur\n--\nphrase: Pour l'anniversaire du voisin, il y avait un gateau aux amandes.\nmot à remplacer: amandes\nparonyme: amendes\nnouveau contexte du paronyme remplaçant: policier\n--\nphrase: "+ sentence_df['sentence'][0] +"\nmot à remplacer: "+ sentence_df['pick_word'][0] +"\nparonyme: "+ sentence_df['swap_word'][0] +"\nnouveau contexte du paronyme remplaçant:",
            "numResults": 10,
            "maxTokens": 10,
            "temperature": 0.7,
            "topKReturn": 0,
            "topP":1,
            "countPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "frequencyPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "presencePenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
        },
        "stopSequences":["--"]
        }
    )
    topics.status_code
    data = topics.json()
    for i in range(10):
        topics_list.append(data['completions'][0]['data']['text'])
    topics_list = list(dict.fromkeys(topics_list))
    sentence_df['topic'] = topics_list
    return sentence_df
