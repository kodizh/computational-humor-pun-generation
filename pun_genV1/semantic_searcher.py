import pandas as pd
import fasttext
import fasttext.util
import numpy as np
import math
import colorama
from colorama import Fore


def start(input_list,initial_word):
    ### input - Word df + close words list ###
    
    word_list = []
    for e in input_list:
        word_list.append(e['word'])

    ### process - find the sementicaly farthest word in the list
    print(Fore.BLUE + "Semantic based reseach : loading model")
    ft = fasttext.load_model('vecfr/cc.fr.300.bin')
    print(Fore.BLUE + "Semantic based reseach : getting main word vector")
    word_vec = ft.get_word_vector(initial_word)


    def vec_cos_angle(vector_1, vector_2):
        unit_vector_1 = vector_1 / np.linalg.norm(vector_1)

        unit_vector_2 = vector_2 / np.linalg.norm(vector_2)

        dot_product = np.dot(unit_vector_1, unit_vector_2)

        return math.cos(np.arccos(dot_product))

    cur_similarity = 1
    cur_word = ""

    print(Fore.BLUE + "Semantic based reseach : researching farthest word in the list")
    for w in word_list:
        vec = ft.get_word_vector(w)
        angle = vec_cos_angle(word_vec,vec)
        if(angle<cur_similarity):
            cur_similarity=angle
            cur_word = w


    print(Fore.BLUE + "Semantic based reseach : The sementicaly farthest word from : " + initial_word +" in the list is : " + cur_word)

        

    ### output - df = sentence + word + [phoneticaly close & semanticaly farthest word] ###

    print(Fore.BLUE + "Semantic based reseach : returning values")
    return cur_word