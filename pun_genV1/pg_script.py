import pandas as pd
import re

import colorama
from colorama import Fore
from regex import T

import semantic_searcher
import topic_j1

import os
clear = lambda: os.system('clear')
clear()                                 ## Clear the terminal before execution

import json
with open('pun_genV1/setup.json') as json_file:
    setup = json.load(json_file)
YOUR_API_KEY = setup["API_KEY"]
PATH_TO_DATASET = setup["path_to_dataset"]
PATH_TO_OUTPUT = setup["path_to_output"]

########################################
###        <sentence_input()>        ###
########################################
# This function is used when the input #
# is not a data set, but a one shot    #
# sentence.                            #
########################################
def sentence_input():
    return input("Enter a sentence : ")

def df_input():
    input_df = pd.read_json(PATH_TO_DATASET)
    print(input_df)
    return input_df

########################################
###     <start(input_sentence)>      ###
########################################
# This start function take an input    #
# sentence as parameter, first the     #
# punctuation is remove, and all words #
# are split. Then the Lexique 3 is     #
# imported, it have to be downloaded   #
# and its place have to follow the     #
# following relative path.             #
########################################
def start(choice):
    
    def isAmbigu(word): # => This method look if a word is ambiguous or not, based on our criteria
        list_pos = []   # a list for parts of speech (pos)
        for i in lex.index: 
            if(lex['ortho'][i] == word): # for the given word
                list_pos.append(lex['cgram'][i]) # add its pos in the list
        if(len(list_pos) > 1):  # if the word have more than one possible pos 
                                # (we do not analyze the sentence to know what 
                                # the current position is, so the word is qualified as ambiguous)
            return True
        else:
            return False
    
    def homophoneOnly(MyList): # => This method take a list of word (dict with word information) and remove all words that have not homophone
        for i in range(len(MyList)): #for each word in the given list
            word = MyList[i]['word']
            for j in lex.index:
                if(lex['ortho'][j] == word): # we check the word row in the lexicon
                    if(lex['nbhomoph'][j] < 2): # The lexicon give the homophone number 
                        MyList.pop(i)  # remove the word from the list
    
    def getLast(pretenders_list): # => This method take the list of predenters and return the closest
                                    # word to the end of the sentence
        last_word = ""
        max_index = 0
        for i in range(len(pretenders_list)): # for each word
            if(pretenders_list[i]['location'] > max_index): # simple comparison
                max_index = pretenders_list[i]['location']
                last_word = pretenders_list[i]['word']
        return last_word
        
    def getPhonList(word): # => this method take the selected word and return the list with all its homophones
        phon_form = ""
        lemme_form = ""
        cgram_form = ""
        freq = 0
        phon_list = []
        for i in lex.index: # this loop permit to init the characteristics of the selected word
            if(lex['ortho'][i] == word):
                phon_form = lex['phon'][i]      # The phonitic form
                lemme_form = lex['lemme'][i]    # The lemmatized form
                cgram_form = lex['cgram'][i]    # The POS

        for j in lex.index:
            if(lex['phon'][j] == phon_form):    # if a word have the save phonetic form than our word (homophone)
                flag1 = lex['lemme'][j] != lemme_form       # 1st flag : Lemmatized form must be different
                flag2 = lex['cgram'][j] == cgram_form       # 2nd flag : POS must be the same
                freq = (int(lex['freqfilms2'][j]) + int(lex['freqlivres'][j])) / 2  # we also need the frequency (here is an average of both frequency fields)
                flag3 = freq > 2                            # 3rd flag : The frequency must be sufficient
                flag = flag1 & flag2 & flag3       # each flags have to be checked    
                if(flag): # if the conditions are met
                    #print(freq)
                    phon_list.append({"word" : lex['ortho'][j],
                                      "phon" : lex['phon'][j],
                                      "cgram" : lex['cgram'][j]}) # The homophone is add into the list
        return phon_list
    
    mydf=pd.DataFrame(columns=['text'], index=[0])
    if(choice == 2):
        input_df = df_input()
        mydf=input_df
    elif(choice == 1):
        input_sentence = sentence_input()
        mydf['text'][0]=input_sentence
        
    res_list = []
    
    for text_id in mydf.index:
        input_sentence = mydf['text'][text_id]
        nopunc_sentence = re.sub("[^'^\w\s]", "", input_sentence)   # remove punctuation
        
        word_list = nopunc_sentence.split()                         # split the sentence into a word list
        
        lex = pd.read_csv('Lexique383/Lexique383.tsv', sep='\t')    # import the french lexicon "Lexique 3"  
        
        pretenders_list=[]  # init a list for words pretending to be puns
        
        print(Fore.RED + input_sentence)
        
        for i in lex.index:
            for word in word_list: # We start by taking a look at all words
                if(lex['ortho'][i] == word):
                    pos = lex['cgram'][i]
                    if((pos == 'NOM' or pos == 'ADJ') & (isAmbigu(word) == False)):     # to check their POS and ambiguousness
                        pretenders_list.append({"pos" : pos, "word" : word})            # the word is added in the list  
                        homophoneOnly(pretenders_list)                                  # pretender words are filtered based on their number of homophones
                        
        print(Fore.MAGENTA + str(pretenders_list))
        # pretenders_list.append({"pos" : "FAKE", "word" : "arrêté"}) #to test with more than one elmt
        # print(len(pretenders_list))
        if(len(pretenders_list) == 0): 
            print(Fore.GREEN + "This sentence has no pretending word for a pun according to our criteria !")
            continue
        if(len(pretenders_list) == 1):
            print(Fore.GREEN + "This sentence has only one potencial word to pun, its position in the text is : " + str(word_list.index(pretenders_list[0]["word"])) + "/" + str(len(word_list)))
        else:
            print(Fore.GREEN + "This sentence has many potencial word to pun : ")
            for i in range(len(pretenders_list)):
                print(Fore.CYAN + str(pretenders_list[i]) + " - location in sentence : " + str(word_list.index(pretenders_list[i]["word"])) + "/" + str(len(word_list)))
        
        
        for i in range(len(pretenders_list)): # each word location is addded in the list
            loc = {"location" : word_list.index(pretenders_list[i]["word"])}
            pretenders_list[i].update(loc)
        
        pretender_word = getLast(pretenders_list) # the best word is always le end-closer-word 
        pretender = {}
        for i in lex.index:
            if(lex['ortho'][i] == pretender_word):
                pretender = {"word" : pretender_word, "phon" : lex['phon'][i], "cgram" : lex['cgram'][i]} #saving the predenter metadata
        print(Fore.RED + "Best pretender is : " + str(pretender))
        phon_list = getPhonList(pretender['word'])      #Geting the list of homophones
        if(len(phon_list) == 0):
            print(Fore.GREEN + "There is no homophone avalible for pun")
            continue
        if(len(phon_list) == 1):
            print(Fore.GREEN + "There is only one homophone")
            farthest_homophone = semantic_searcher.start(phon_list,pretender['word']) 
            print(Fore.RED + "The farthest homophone is : " + str(farthest_homophone))
        if(len(phon_list) > 1):
            print(Fore.GREEN + "There is many homophone !!")
            print(Fore.MAGENTA + str(phon_list))
            farthest_homophone = semantic_searcher.start(phon_list,pretender['word'])       #Getting the semanticaly farthest word
            print(Fore.RED + "The farthest homophone is : " + str(farthest_homophone))

        new_sentence = {'sentence' : input_sentence, 'pick_word' : pretender_word, 'swap_word' : farthest_homophone}
        
        new_sentence = topic_j1.topic(new_sentence,YOUR_API_KEY)
        
        print(new_sentence)
        
        res_list.append(new_sentence)
        print(res_list)
        df = pd.DataFrame(res_list)
        
        df.to_json(PATH_TO_OUTPUT)

    
def main():


    choice = int(input("Select an input mode (1:sentence/2:dataset): "))
    if(choice == 1 or choice == 2):
        start(choice)
    else:
        print("Error : wrong choice value")
    
    
    #start(input_sentence)
    
if __name__ == "__main__":
    main()
